function display(pZ)
% display - displays a polyZonotope object to the console
%
% Syntax:  
%    display(pZ)
%
% Inputs:
%    pZ - polyZonotope object
%
% Outputs:
%    -
%
% Example: 
%    pZ = polyZonotope([2;1],[1 0; -2 1],[1; 0],[0 2; 1 0])
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Mark Wetzlinger
% Written:       02-May-2020
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------

if isempty(pZ)
    
    dispEmptyObj(pZ,inputname(1));
    
else
    
    fprintf(newline);
    disp(inputname(1) + " =");
    fprintf(newline);
    
    disp("     c:      [" + size(pZ.c,1) + "x" + size(pZ.c,2) + "] double");
    disp("     G:      [" + size(pZ.G,1) + "x" + size(pZ.G,2) + "] double");
    disp("     Grest:  [" + size(pZ.Grest,1) + "x" + size(pZ.Grest,2) + "] double");
    disp("     expMat: [" + size(pZ.expMat,1) + "x" + size(pZ.expMat,2) + "] double");
    disp("     id:     [" + size(pZ.id,1) + "x" + size(pZ.id,2) + "] double");
    
    fprintf(newline);
    
end

%------------- END OF CODE --------------

