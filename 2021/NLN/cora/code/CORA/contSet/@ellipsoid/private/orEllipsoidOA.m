function E = orEllipsoidOA(E,E_cell)
% minus - Computes the outer approximation of the union between
%           ellipsoids
%
% Syntax:  
%    E = unionEllipsoidOA(E,E_cell)
%
% Inputs:
%    E1 - ellipsoid object
%    E_cell - ellipsoid array
%
% Outputs:
%    E - ellipsoid after union
%
% References:
%   [1] Boyd et al. Convex Optimization
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gassmann
% Written:      15-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
% collapse cell array
E_cell = [{E};E_cell];
N = length(E_cell);
n = E.dim;

% check if all "dimensions" are covered by E_cell
Q_sum = zeros(n);
TOL = min(cellfun(@(e)e.TOL,E_cell));
for i=1:N
    Q_sum = Q_sum + E_cell{i}.Q;
end

T = eye(n);
nt = n;
n_d = 0;
x_rem = zeros(n_d,1);
E_sum = ellipsoid(Q_sum);
if E_sum.isdegenerate
    % all elements in E_cell are degenerate and diagonalizing with V
    % exposes common "0" dimension
    % => transform all ellipsoids so that at least one fulldimensional is
    % contained in E_cell
    [T,~,~] = svd(Q_sum);
    n_d = E_sum.rank;
    nt = n - n_d;
    x_rem = zeros(n_d,1);
    for i=1:N
        x_rem = x_rem + E_cell{i}.q(nt+1:end);
        E_cell{i} = project(T'*E_cell{i},1:nt);
    end
    if n_d==n
        % all Qi's zero
        E = T*ellipsoid(zeros(n),x_rem);
        return;
    end
end

for i=1:N
    Ei = E_cell{i};
    if Ei.isdegenerate
        % if Ei still degenerate, add small pertubation (possible since we
        % compute an overapproximation)
        [Ti,Si,~] = svd(Ei.Q);
        si = diag(Si);
        % choose singular value such that rcond not too small
        nd_i = Ei.rank;
        % bloat to remove degeneracy
        Si = diag([si(1:nd_i);0.01*max(si)*ones(nt-nd_i,1)]);
        E_cell{i} = ellipsoid(Ti*Si*Ti',Ei.q);
    end
end

% find minimum volume ellipsoid spanning union [1]
A2 = sdpvar(nt); % pd-ness of A ensured by yalmip internally
bt = sdpvar(nt,1);
l = sdpvar(N,1);
f_obj = -1/2*geomean(A2);
C = [];
for i=1:N
    Qiinv = inv(E_cell{i}.Q);
    qi = E_cell{i}.q;
    Ci = [A2-l(i)*Qiinv,    bt+l(i)*Qiinv*qi,            zeros(nt);
          (bt+l(i)*Qiinv*qi)',-1-l(i)*(qi'*Qiinv*qi-1),  bt';
          zeros(nt),          bt,                         -A2];
    C = [C, Ci<=0];
end
opts = sdpsettings;
opts.verbose = 0;
if exist('sdpt3','file')
    opts.solver = 'sdpt3';
end
sol = optimize([C,l>=0],f_obj,opts);
if any(sol.problem == [-2,-3,-4])
    error('This function requires a SDP solver compatible with YALMIP');
elseif sol.problem ~= 0
    error('Problem solving optimization problem.');
end

% extract ellipsoid
Qt = inv(value(A2));
A = sqrtm(value(A2));
b = A\value(bt);
qt = -Qt*A*b;

% backtransform
E = T'*ellipsoid([Qt,zeros(nt,n_d);zeros(n_d,n)],[qt;x_rem]);
%------------- END OF CODE --------------