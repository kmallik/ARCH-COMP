function main()

    addpath(genpath('../code'));

    % Install MP Toolbox ------------------------------------------------------------
    
    cd MultiPrecisionToolbox/@mp/private
    
    mp_compile_all
    
    cd ../../..


    % Install MPT Toolbox ------------------------------------------------------------

    % create installation directory
    mkdir('tbxmanager')
    cd tbxmanager

    % install toolbox-manager
    urlwrite('http://www.tbxmanager.com/tbxmanager.m', 'tbxmanager.m');
    tbxmanager
    savepath

    % install mpt-toolbox
    tbxmanager install mpt mptdoc cddmex fourier glpkmex hysdel lcp sedumi espresso

    % initialize toolbox
    mpt_init
    cd ..

    
    % initialize .csv file and track command-line outputs
    fid = fopen('../results/results.csv','w');
    diary '../results/computationTime'


    % HEAT 3D Benchmark -------------------------------------------------------------

    disp('Heat 3D -------------------------------------------------------------------');
    
    disp(' ');
    disp('HEAT01 ------------------');
    text = example_linear_reach_ARCH21_heat3D_HEAT01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/HEAT01.png');
    close;
    
    disp(' ');
    disp('HEAT02 ------------------');
    text = example_linear_reach_ARCH21_heat3D_HEAT02();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/HEAT02.png');
    close;
    
    disp(' ');
    disp('HEAT03 ------------------');
    text = example_linear_reach_ARCH21_heat3D_HEAT03();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/HEAT03.png');
    close;

    
    % Beam Benchmark ----------------------------------------------------------------

%     disp('Beam ----------------------------------------------------------------------');
%     
%     disp(' ');
%     disp('CB01 ------------------');
%     text = example_linear_reach_ARCH21_beam_CB100();
%     fprintf(fid,'%s\n',text);
%     saveas(gcf, '../results/CB01.png');
%     close;
%     
%     disp(' ');
%     disp('CB02 ------------------');
%     text = example_linear_reach_ARCH21_beam_CB500();
%     fprintf(fid,'%s\n',text);
%     saveas(gcf, '../results/CB02.png');
%     close;
%     
%     disp(' ');
%     disp('CB03 ------------------');
%     text = example_linear_reach_ARCH21_beam_CB1000();
%     fprintf(fid,'%s\n',text);
%     saveas(gcf, '../results/CB03.png');
%     close;
    

    % ISS Benchmark -----------------------------------------------------------------

    disp(' ');
    disp(' ');
    disp('International Space Station -----------------------------------------------');
    
    disp(' ');
    disp('ISSF01-ISS01 ------------');
    text = example_linear_reach_ARCH21_iss_ISSF01_ISS01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/ISSF01_ISS01.png');
    close
    
    disp(' ');
    disp('ISSF01-ISU01 ------------');
    text = example_linear_reach_ARCH21_iss_ISSF01_ISU01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/ISSF01_ISU01.png');
    close
    
    disp(' ');
    disp('ISSC01-ISS02 ------------');
    text = example_linear_reach_ARCH21_iss_ISSC01_ISS02();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/ISSC01_ISS02.png');
    close
    
    disp(' ');
    disp('ISSC01-ISU02 ------------');
    text = example_linear_reach_ARCH21_iss_ISSC01_ISU02();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/ISSC01_ISU02.png');
    close



    % Spacecraft Rendezvous ---------------------------------------------------------

    disp(' ');
    disp(' ');
    disp('Spacecraft Rendezvous -----------------------------------------------------');
    
    disp(' ');
    disp('SRNA01 ------------------');
    text = example_hybrid_reach_ARCH21_rendezvous_SRNA01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRNA01.png');
    close
    
    disp(' ');
    disp('SRA01 -------------------');
    text = example_hybrid_reach_ARCH21_rendezvous_SRA01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA01.png');
    close
    
    disp(' ');
    disp('SRA02 -------------------');
    text = example_hybrid_reach_ARCH21_rendezvous_SRA02();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA02.png');
    close
    
    disp(' ');
    disp('SRA03 -------------------');
    text = example_hybrid_reach_ARCH21_rendezvous_SRA03();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA03.png');
    close
    
    disp(' ');
    disp('SRA04 -------------------');
    text = example_hybrid_reach_ARCH21_rendezvous_SRA04();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA04.png');
    close

    disp(' ');
    disp('SRA05 -------------------');
    text = example_hybrid_reach_ARCH21_rendezvous_SRA05();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA05.png');
    close
    
    disp(' ');
    disp('SRA06 -------------------');
    text = example_hybrid_reach_ARCH21_rendezvous_SRA06();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA06.png');
    close
    
    disp(' ');
    disp('SRA07 -------------------');
    text = example_hybrid_reach_ARCH21_rendezvous_SRA07();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA07.png');
    close
    
    disp(' ');
    disp('SRA08 -------------------');
    text = example_hybrid_reach_ARCH21_rendezvous_SRA08();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRA08.png');
    close
    
    disp(' ');
    disp('SRU01 -------------------');
    text = example_hybrid_reach_ARCH21_rendezvous_SRU01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRU01.png');
    close
    
    disp(' ');
    disp('SRU02 -------------------');
    text = example_hybrid_reach_ARCH21_rendezvous_SRU02();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/SRU02.png');
    close
    
    
    
    % Powertrain --------------------------------------------------------------------

    disp(' ');
    disp(' ');
    disp('Powertrain ----------------------------------------------------------------');
    
    disp(' ');
    disp('DTN01 -------------------');
    text = example_hybrid_reach_ARCH21_powerTrain_DTN01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/DTNO1.png');
    close
    
    disp(' ');
    disp('DTN02 -------------------');
    text = example_hybrid_reach_ARCH21_powerTrain_DTN02();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/DTNO2.png');
    close
    
    disp(' ');
    disp('DTN03 -------------------');
    text = example_hybrid_reach_ARCH21_powerTrain_DTN03();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/DTNO3.png');
    close
    
    disp(' ');
    disp('DTN04 -------------------');
    text = example_hybrid_reach_ARCH21_powerTrain_DTN04();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/DTNO4.png');
    close
    
    disp(' ');
    disp('DTN05 -------------------');
    text = example_hybrid_reach_ARCH21_powerTrain_DTN05();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/DTNO5.png');
    close
    
    disp(' ');
    disp('DTN06 -------------------');
    text = example_hybrid_reach_ARCH21_powerTrain_DTN06();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/DTNO6.png');
    close
    
    
    % Building -----------------------------------------------------------------------

    disp(' ');
    disp(' ');
    disp('Building -------------------------------------------------------------------');
    
    disp(' ');
    disp('BLDC01 ------------------');
    text = example_linear_reach_ARCH21_building_BLDC01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/BLDC01_1.png');
    close(gcf);
    saveas(gcf, '../results/BLDC01_2.png');
    close(gcf);
    
    disp(' ');
    disp('BLDF01 ------------------');
    text = example_linear_reach_ARCH21_building_BLDF01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/BLDF01_1.png');
    close(gcf);
    saveas(gcf, '../results/BLDF01_2.png');
    close(gcf);
    
    
    
    % Platoon ------------------------------------------------------------------------

    disp(' ');
    disp(' ');
    disp('Platoon --------------------------------------------------------------------');
    
    disp(' ');
    disp('PLAA01-BND50 ------------');
    text = example_linearParam_reach_ARCH21_platoon_PLAA01_BND50();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/PLAA01_BND50.png');
    close(gcf);
    
    disp(' ');
    disp('PLAA01-BND42 ------------');
    text = example_linearParam_reach_ARCH21_platoon_PLAA01_BND42();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/PLAA01_BND42.png');
    close(gcf);
    
    disp(' ');
    disp('PLAD01-BND42 ------------');
    text = example_linearParam_reach_ARCH21_platoon_PLAD01_BND42();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/PLAD01_BND42.png');
    close(gcf);
    
    disp(' ');
    disp('PLAD01-BND30 ------------');
    text = example_linearParam_reach_ARCH21_platoon_PLAD01_BND30();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/PLAD01_BND30.png');
    close(gcf);
    
    disp(' ');
    disp('PLAN01 ------------------');
    text = example_linearParam_reach_ARCH21_platoon_PLAN01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/PLAN01.png');
    close(gcf);
    
    
    % Gearbox ------------------------------------------------------------------------

    disp(' ');
    disp(' ');
    disp('Gearbox --------------------------------------------------------------------');
    
    disp(' ');
    disp('GRBX01-MES01 ------------');
    text = example_hybrid_reach_ARCH21_gearbox_GRBX01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/GRBX01_MES01.png');
    close(gcf);
    
    disp(' ');
    disp('GRBX02-MES01 ------------');
    text = example_hybrid_reach_ARCH21_gearbox_GRBX02();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/GRBX02_MES01.png');
    close(gcf);
    
    
    % Brake --------------------------------------------------------------------------

    disp(' ');
    disp(' ');
    disp('Brake ----------------------------------------------------------------------');
    
    disp(' ');
    disp('BRKDC01 -----------------');
    text = example_hybrid_reach_ARCH21_brake_BRKDC01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/BRKDC01.png');
    close(gcf);
    
    disp(' ');
    disp('BRKNC01 -----------------');
    text = example_hybrid_reach_ARCH21_brake_BRKNC01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/BRKNC01.png');
    close(gcf);
    
    disp(' ');
    disp('BRKNP01 -----------------');
    text = example_hybrid_reach_ARCH21_brake_BRKNP01();
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/BRKNP01.png');
    close(gcf);
  
    
    % Close .csv file
    fclose(fid);

end