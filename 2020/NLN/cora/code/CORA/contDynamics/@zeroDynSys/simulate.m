function [t,x,ind] = simulate(obj,params,options)
% simulate - simulates the system within a location
%
% Syntax:  
%    [t,x,ind] = simulate(obj,params,options)
%
% Inputs:
%    obj - linearSys object
%    params - struct containing the parameters for the simulation
%       .tFinal: final time
%    options - ODE45 options (for hybrid systems)
%
% Outputs:
%    obj - linearSys object
%    t - time vector
%    x - state vector
%    index - returns the event which has been detected
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Matthias Althoff
% Written:       02-November-2007 
% Last update:   08-May-2020 (MW, update interface)
% Last revision: ---

%------------- BEGIN CODE --------------

t=params.tFinal;
x=x0';
te=[];
xe=[];
ind=[];

%------------- END OF CODE --------------