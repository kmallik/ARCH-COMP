function [subscriptMatrix]=cellSegments(Obj,indexVector)
% DEPRECATED -- FUNCTIONALITY PROVIDED BY segmentSubscripts.m (POSSIBLE
% DIFFERENT FORMAT OF INDEX VECTOR DUE TO NEW CONVENTION. SEE
% @partition/i2s.m FOR DETAILS.
% Purpose:  accesses private i2s function
% Pre:      partition object, index vector
% Post:     cell segments
% Built:    27.03.08,MA


subscriptMatrix=i2s(Obj,indexVector);