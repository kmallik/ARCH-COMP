from abc import ABCMeta, abstractmethod

NOT_IMPLEMENTED = "Needs to be implemented."


class Agent:
    """ Abstract Agent class.

    Responsible for computing the agent's next action given a state in the
    environment.
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_constraints_for_action(self, constrs_manager, input_state_vars):
        """
        Get the constraints for the network itself, set constraints to
        determine the action taken by the agent. Will need to compute the
        action of the agent based on the Q-value with highest value.
        :param constrs_manager: Constraint manager handling all constraints.
        :param input_state_vars: Set of variables representing the state that
        is input to the agent's protocol function.
        :side-effects: May modify constrs_manager.
        :return:
            variables representing action taken,
            the constraints and
            the binary variables used in indicator constraints.
        """
        return NotImplementedError(NOT_IMPLEMENTED)
