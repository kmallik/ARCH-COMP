function [Zred]=deleteZeros(Z)
% deleteZeros - removes zero generators
%
% Syntax:  
%    [Zred]=deleteZeros(Z)
%
% Inputs:
%    Z - zonotope object
%
% Outputs:
%    Zred - reduced zonotope object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: 

% Author:       Matthias Althoff
% Written:      15-January-2009
% Last update:  27-Aug-2019
% Last revision: ---

%------------- BEGIN CODE --------------

%extract center and generator matrix
c=center(Z);
G=generators(Z);

%Delete zero-generators
G=nonzeroFilter(G);

Zred=zonotope([c,G]);

%------------- END OF CODE --------------
