#!/bin/bash 
function run {
	echo $2 ":"
	docker run  -v `pwd`:`pwd` -w `pwd` spaceex -g $1$2.cfg -m $1$3.xml -vl | grep 'Forbidden\|Computing reachable states done after'
}

echo "ISS"
run SpaceStation/ ISSF01-ISS01 ISSF01
run SpaceStation/ ISSF01-ISU01 ISSF01
run SpaceStation/ ISSC01-ISS02 ISSC01
run SpaceStation/ ISSC01-ISU02 ISSC01
echo "done."

echo "Spacecraft Rendezvous"
run Rendezvous/ SRNA01-SR02 SRNA01-SR0_
run Rendezvous/ SRA01-SR02 SRA01-SR0_
run Rendezvous/ SRU01-SR02 SRU01-SR0_
run Rendezvous/ SRU02-SR02 SRU02-SR0_
# Attention: the following instances seem to be wrong
#run Rendezvous/ SRA02-SR02 SRA01-SR0_
#run Rendezvous/ SRA03-SR02 SRA01-SR0_
#run Rendezvous/ SRA04-SR02 SRA01-SR0_
#run Rendezvous/ SRA05-SR02 SRA01-SR0_
#run Rendezvous/ SRA06-SR02 SRA01-SR0_
#run Rendezvous/ SRA07-SR02 SRA01-SR0_
#run Rendezvous/ SRA08-SR02 SRA01-SR0_
echo "done."

echo "Building"
run Building/ BLDC01-BDS01 BLDC01
run Building/ BLDF01-BDS01 BLDF01
#run Building/ BLDF01-BDU01 BLDF01
#run Building/ BLDF01-BDU02 BLDF01
#run Building/ BLDC01-BDU02 BLDC01
echo "done."

echo
echo "Platoon"
run Platoon/ PLAD01-BND42 PLAD01-BND
run Platoon/ PLAD01-BND30 PLAD01-BND
run Platoon/ PLAN01-UNB50 PLAN01-UNB
echo "done."

echo
echo "Gearbox"
run Gear/ GRBX01-MES01 GRBX01-MES01
run Gear/ GRBX01-MES02 GRBX01-MES01
echo "done."


