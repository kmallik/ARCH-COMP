function pass = test_spaceex2cora_linear_building()
% test_spaceex2cora_linear_building - example of linear reachability 
% analysis from the ARCH17 friendly competition (building example)
%
% Syntax:  
%    test_spaceex2cora_linear_building
%
% Inputs:
%    no
%
% Outputs:
%    pass - boolean 
%
% Example: 
%
% 
% Author:       Matthias Althoff, Raja Judeh
% Written:      09-February-2017
% Last update:  02-September-2018
% Last revision:15-January-2019 (SX_HA to SX_lin)


%------------- BEGIN CODE --------------

%% File loading

G = load('build'); %loads the struct 'G' from file 'build'

%% Set options

R0 = interval([0.0002*ones(10,1); -0.0001; zeros(37,1)], ... 
              [0.00025*ones(10,1); 0.0001; zeros(37,1)]); %'interval' obj
          
options.x0 = mid(R0); %returns the center of the interval R0
options.taylorTerms = 6; %number of taylor terms for reachable sets
options.zonotopeOrder = 100;  %zonotope order
options.originContained = 0;
options.reductionTechnique = 'girard';
options.linAlg = 1;
uTrans = 0.9;
options.u = uTrans;
options.uTrans = uTrans; %center of input set
options.U = zonotope([options.uTrans,0.1]); %input for reachability analysis
options.path = [coraroot '/contDynamics/stateSpaceModels']; %returns the CORA root path

%% Specify continuous dynamics

% Initialize system
buildingSys = linearSys('buildingSys', G.A, G.B); %original system

% Convert the 'build_48.xml' file into a CORA model called 'build_48'
spaceex2cora('build_48.xml')
buildingSysSX = build_48(); %returns a 'hybridAutomation' obj

%% Compute reachable set using zonotopes

options.R0 = zonotope(R0); %initial state for reachability analysis
timeStep_1 = 0.002;
options.timeStep = timeStep_1;
options.tStart = 0; %start time
options.tFinal = 2; %final time

% Simulate the original system
[~,simRes.t,simRes.x] = simulate(buildingSys, options, options.tStart, ...
                                 options.tFinal, options.x0, options);

% Simulate the system converted from spaceEX
[~,simResSX.t,simResSX.x] = simulate(buildingSysSX, options, options.tStart, ...
                  options.tFinal, options.x0, options);

%% Compute error between the simulation of the two files

num_channels = size(simRes.x, 2); %number of channels in the system

for ch = 1:num_channels
    diff = simRes.x(:,ch) - simResSX.x(:,ch);
    error = norm(diff); %length of the difference vector

    if(error > 1e-5)
        disp('Failed Conversion: error = ' + string(error));
        pass = false;
        return
    end
end

disp('Sucessful Conversion: error = ' + string(error))
pass = true;

%------------- END OF CODE --------------

end


    