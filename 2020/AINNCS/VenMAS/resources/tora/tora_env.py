from src.actors.envs.environment import AbstractEnvironment
from src.network_parser.network_model import NetworkModel
from src.verification.bounds.bounds import HyperRectangleBounds


class ToraEnv(AbstractEnvironment):

    def __init__(self, sin_model):
        """
        Environment for the Tora benchmark
        """
        assert isinstance(sin_model, NetworkModel)

        # set the branching factor to 1
        super(ToraEnv, self).__init__(1)

        self.sin_model = sin_model

        self.dt = 1

        # w does not seem to be used in the ODEs
        self.w_lower = - 0.01
        self.w_upper = 0.01

    def get_constraints_for_transition(self, i, constrs_manager, action_vars, input_state_vars):
        """
        :param i:
        :param constrs_manager:
        :param action_vars:
        :param input_state_vars: variables representing the current state
        :return:
        """

        constrs = []

        [x1, x2, x3, x4] = input_state_vars
        [u] = action_vars

        # Get and add the constraint for computing sine.
        [sin_x3], sin_constrs = \
            constrs_manager.get_network_constraints(self.sin_model.layers, [x3])

        # Compute bounds for next state variables
        bounds = constrs_manager.get_variable_bounds([x1, x2, x3, x4, u])
        sin_bounds = constrs_manager.get_variable_bounds([sin_x3])



        next_x1_lower, next_x1_upper = self.next_x1_bounds(bounds.get_lower(), bounds.get_upper())
        next_x2_lower, next_x2_upper = self.next_x2_bounds(bounds.get_lower(), bounds.get_upper(), sin_bounds.get_lower(),sin_bounds.get_upper())

        next_x3_lower, next_x3_upper = self.next_x3_bounds(bounds.get_lower(), bounds.get_upper())
        next_x4_lower, next_x4_upper = self.next_x4_bounds(bounds.get_lower(), bounds.get_upper())


        # Create next state variables with computed bounds
        [next_x1, next_x2,next_x3,next_x4] = \
            constrs_manager.create_state_variables(4,
                                                   lbs=[next_x1_lower, next_x2_lower, next_x3_lower, next_x4_lower],
                                                   ubs=[next_x1_upper,next_x2_upper, next_x3_upper, next_x4_upper])
        constrs_manager.add_variable_bounds([next_x1, next_x2,next_x3,next_x4],
                                            HyperRectangleBounds([next_x1_lower, next_x2_lower, next_x3_lower, next_x4_lower],
                                                                 [next_x1_upper,next_x2_upper, next_x3_upper, next_x4_upper]))


        # Add the constraints for computing next_x1 and others

        # next_x1 == x1 + x2 *self.dt
        constrs.append(constrs_manager.get_linear_constraint([next_x1, x1, x2],
                                                             [-1, 1, self.dt],
                                                             0))

        # next_x2 == x2 - x1 * dt + 0.1 * sin(x3) * dt
        constrs.append(constrs_manager.get_linear_constraint([next_x2, x2, x1, sin_x3],
                                                             [-1, 1, -1 * self.dt, 0.1 * self.dt],
                                                             0))

        # next_x3 == x3  + x4 * self.dt
        constrs.append(
            constrs_manager.get_linear_constraint([next_x3, x3, x4],
                                                  [-1, 1, self.dt],
                                                  0))

        # next_x4 == x4 + u * self.dt
        constrs.append(constrs_manager.get_linear_constraint([next_x4, x4, u],
                                                             [-1, 1, self.dt],
                                                             0))


        return [next_x1, next_x2,next_x3,next_x4], constrs


    def next_x1_bounds(self, input_lower, input_upper):
        """
        """
        # x1(t+1) = x1(t) + x2(t) * dt
        upper = input_upper[0] + self.dt * input_upper[1]
        lower = input_lower[0] + self.dt * input_lower[1]
        return lower, upper

    def next_x2_bounds(self, input_lower, input_upper, sin_lower, sin_upper):
        """
        """
        # x2(t+1) = x2(t) - x1(t) * dt + 0.1 * sin(x3(t)) * dt
        upper = input_upper[1] - input_upper[0] * self.dt + 0.1 * self.dt * sin_upper[0]
        lower = input_lower[1] - input_lower[0] * self.dt + 0.1 * self.dt * sin_lower[0]
        return lower, upper

    def next_x3_bounds(self, input_lower, input_upper):
        """
        """
        # x3(t+1) = x3(t) + x4(t) * dt
        upper = input_upper[2] + input_upper[3] * self.dt
        lower = input_lower[2] + input_lower[3] * self.dt
        return lower, upper

    def next_x4_bounds(self, input_lower, input_upper):
        """
        """
        # x4(t+1) = x4(t) + u(t) * dt
        upper = input_upper[3] + self.dt * input_upper[-1]
        lower = input_lower[3] + self.dt * input_lower[-1] 
        return lower, upper
