function check_lagrangeRem(options, obj)
% check_lagrangeRem - checks if options.lagrangeRem
%  1) takes an allowed value
%
% Syntax:
%    check_lagrangeRem(options, obj)
%
% Inputs:
%    options - options for object
%    obj     - system object
%
% Outputs:
%    -
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   -

% Author:       Mark Wetzlinger
% Written:      04-Mar-2019
% Last update:  03-May-2020 (rewriting of error msgs using class(obj))
% Last revision:---

%------------- BEGIN CODE --------------

strct = 'options';
% lagrangeRem + lagrangeRem.method: either 'interval' or 'taylorModel'
if isfield(options, 'lagrangeRem')
    if ~isfield(options.lagrangeRem,'method')
        error(printOptionMissing(obj,'lagrangeRem.method',strct));
    else
        if ~strcmp(options.lagrangeRem.method,'interval')
            if ~strcmp(options.lagrangeRem.method,'taylorModel')
                error(printOptionOutOfRange(obj,'lagrangeRem.method',strct));
            end
        end
    end
end

end

%------------- END OF CODE --------------

