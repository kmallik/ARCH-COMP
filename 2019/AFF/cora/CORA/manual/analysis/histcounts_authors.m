function HC = histcounts_authors(sub_directory, histo)
% histcounts_authors - counts the number of files each author contributed in the 
% given directory
%
% Syntax:  
%    histcounts_authors(sub_directory)
%
% Inputs:
%       sub_directory: a string contains the directory of the file for wich
%       a histogram is to be generated.
%
%       histo: a Boolean input. If true, a histogram is generated. 
%
% Outputs:
%       HC: a categorical matrix containg two rows: the first row has the 
%       names of the authors that contributed in the given directory. The 
%       second row has the number of files each author contributed in.
%
% 
% Author:       Raja Judeh
% Written:      February-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

%% Loading the list of names that contain no misspellings

acceptable_names = load('acceptable_names.mat'); 
acceptable_names = acceptable_names.acceptable_names;

%% Extract the authors that have no misspellings and generate the histogram

% Extract all the authors in the current sub-directory
authors = get_authors(sub_directory); 

% Join all the authors and then split them again. This solves the situation
% where there is more than one author in the same file
authors = strjoin({authors.author_name}, ','); 
authors = split(authors, ',');
authors = strtrim(authors); %trim any extra spaces

% Extract the names that are only availabe in the 'acceptable_names' list.
% This removes any names that are misspelled or have any other errors.
authors = authors(ismember(authors, acceptable_names)); 

% Convert the names into categorical form in order to generate a histogram
authors = categorical(authors);
[counts, names] = histcounts(authors);
names = categorical(names);
counts = categorical(counts);

if histo == true
    hist(authors) %generate the histogram
end

HC = [names; counts];

%------------- END OF CODE --------------

end