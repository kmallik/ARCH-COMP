#/bin/sh
 
docker run --rm --workdir /code --mac-address=8c:16:45:19:6c:93 --volume "$PWD/license.lic":/MATLAB/licenses/network.lic --volume "$PWD/data":/data --volume "$PWD/code":/code --volume "$PWD/results":/results registry.codeocean.com/codeocean/matlab:2019a-ubuntu18.04 /bin/bash -c "who; chmod +x run; ./run"
