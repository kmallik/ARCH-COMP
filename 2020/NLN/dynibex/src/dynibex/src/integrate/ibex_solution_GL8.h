/* ============================================================================
 * D Y N I B E X - Definition of the GAUSS8 Method
 * ============================================================================
 * Copyright   : ENSTA ParisTech
 * License     : This program can be distributed under the terms of the GNU LGPL.
 *               See the file COPYING.LESSER.
 *
 * Author(s)   : Julien Alexandre dit Sandretto and Alexandre Chapoutot
 * Created     : June 24, 2015
 * Sponsored   : This research benefited from the support of the "Chair Complex Systems Engineering - Ecole Polytechnique, THALES, DGA, FX, DASSAULT AVIATION, DCNS Research, ENSTA ParisTech, Telecom ParisTech, Fondation ParisTech and FDO ENSTA"
 * ---------------------------------------------------------------------------- */
#ifndef IBEX_SOL_GAUSS8_H
#define IBEX_SOL_GAUSS8_H

#include <iomanip>
#include <stdlib.h>
#include "ibex_solution.h"

//#ifndef _COMPUTATION_WITH_DAGS
//#define _COMPUTATION_WITH_DAGS
//#endif

namespace ibex{

  class solution_j_gauss8 : public solution_j
  {
  public:

    //method to define

    IntervalVector picard(IntervalVector y0, ivp_ode* _ode, int ordre)
    {
      return picard_tayl(y0,_ode,ordre);
    }

    //the LTE
    Affine2Vector LTE(IntervalVector y0,ivp_ode* _ode, double h)
    {
//#ifdef 	_COMPUTATION_WITH_DAGS
if (_ode->get_lte_computation() == AUTODIF){
      if (!_ode->get_dags()->is_filled_in()){


	// cerr << "GAUSS8 avec LTE DAGS" << endl;
	ibex::IntervalMatrix A(4, 4, 0.0);
	A[0][0] = a11;
	A[0][1] = a12;
	A[0][2] = a13;
	A[0][3] = a14;

	A[1][0] = a21;
	A[1][1] = a22;
	A[1][2] = a23;
	A[1][3] = a24;

	A[2][0] = a31;
	A[2][1] = a32;
	A[2][2] = a33;
	A[2][3] = a34;

	A[3][0] = a41;
	A[3][1] = a42;
	A[3][2] = a43;
	A[3][3] = a44;

	ibex::IntervalVector b(4);
	b[0] = b1;
	b[1] = b2;
	b[2] = b3;
	b[3] = b4;
	// cout << A << endl << b << endl;
	_ode->set_Butcher_table(A, b);
      }
      Affine2Vector y0_aff = Affine2Vector(y0);
      Affine2Vector err_aff = _ode->get_dags()->get_lte(9, y0_aff);
      return err_aff;
      }
//#else
      Affine2Vector err_aff = _ode->computeGL8derivative_aff(Affine2Vector(y0,true));
//#endif
      err_aff*=(std::pow(h,9) / 362880.0);

      return err_aff;
    }

    //the factor for the next stepsize computation
    double factor_stepsize(double test)
    {
      return std::min(1.8,std::max(0.4,0.9*std::pow(1.0/test,0.12)));
    }


    //compute the sharpest jn+1
    int calcul_jnh(ivp_ode* _ode){
      //with GAUSS8 and affine form
      *box_jnh_aff = remainder_gauss8(_ode);
      return 1;
    };


    //constructor
  solution_j_gauss8(const Affine2Vector _box_jn, double tn, double h, ivp_ode* _ode,double a, double fac) : solution_j(_box_jn, tn, h, _ode, a, fac)
      {

      }


    //destructor
    ~solution_j_gauss8(){
    }



  private:

    const Interval omega1 = Interval(1.0) / Interval (8.0)
      - sqrt(Interval(30.0)) / Interval(144.0);
    const Interval omega1p = Interval(1.0) / Interval (8.0)
      + sqrt(Interval(30.0)) / Interval(144.0);

    const Interval omega2 = 0.5 * sqrt((15.0 + 2.0 * sqrt(Interval(30.0)))/
				       Interval(35.0));
    const Interval omega2p = 0.5 * sqrt((15.0 - 2.0 * sqrt(Interval(30.0)))/
					Interval(35.0));

    const Interval omega3 = omega2 *
      (Interval(1.0) / Interval (6.0)
       + sqrt(Interval(30.0) / Interval(24.0)));

    const Interval omega3p = omega2p *
      (Interval(1.0) / Interval (6.0)
       - sqrt(Interval(30.0) / Interval(24.0)));

    const Interval omega4 = omega2 *
      (Interval(1.0) / Interval (21.0)
       + 5 * sqrt(Interval(30.0) / Interval(168.0)));

    const Interval omega4p = omega2p *
      (Interval(1.0) / Interval (21.0)
       - 5 * sqrt(Interval(30.0) / Interval(168.)));

    const Interval omega5 = omega2 - 2 * omega3;
    const Interval omega5p = omega2p - 2 * omega3p;


    const Interval a11 = omega1;
    const Interval a12 = omega1p - omega3 + omega4p;
    const Interval a13 =  omega1p - omega3 - omega4p;
    const Interval a14 =  omega1 - omega5;

    const Interval a21 = omega1 - omega3p + omega4;
    const Interval a22 =  omega1p;
    const Interval a23 = omega1p - omega5p;
    const Interval a24 = omega1 - omega3p - omega4;

    const Interval a31 = omega1 + omega3p + omega4;
    const Interval a32 = omega1p + omega5p;
    const Interval a33 =  omega1p;
    const Interval a34 = omega1 + omega3p - omega4;

    const Interval a41 =  omega1 + omega5;
    const Interval a42 = omega1p + omega3 + omega4p;
    const Interval a43 = omega1p + omega3 - omega4p;
    const Interval a44 = omega1;

    const Interval b1 = 2.0 * omega1;
    const Interval b2 = 2.0 * omega1p;
    const Interval b3 = b2;
    const Interval b4 = b1;


    //gauss8 with remainder
    Affine2Vector remainder_gauss8(ivp_ode* _ode)
    {
      double h=time_j.diam();
      double tol = 1e-20;//atol*0.01;

      IntervalVector k1 = _ode->compute_derivatives(1,*box_j1);
      std::cout <<"initial ki : " << k1 << std::endl;
      IntervalVector k2(k1);
      IntervalVector k3(k1);
      IntervalVector k4(k1);


      IntervalVector k1_old(k1);
      IntervalVector k2_old(k2);
      IntervalVector k3_old(k3);
      IntervalVector k4_old(k4);

	std::cout << "begin of contraction" << std::endl;
	int k=0;
      do
	{
	k++;
	  k1_old=k1;
	  k2_old=k2;
	  k3_old=k3;
	  k4_old=k4;


	  k1 &= _ode->compute_derivatives(1,*box_jn + h*(a11*k1 + a12*k2 +
							 a13*k3 + a14*k4));
	  k2 &= _ode->compute_derivatives(1,*box_jn + h*(a21*k1 + a22*k2 +
							 a23*k3 + a24*k4));
	  k3 &= _ode->compute_derivatives(1,*box_jn + h*(a31*k1 + a32*k2 +
							 a33*k3 + a34*k4));

	  k4 &= _ode->compute_derivatives(1,*box_jn + h*(a41*k1 + a42*k2 +
							 a43*k3 + a44*k4));

	  std::cout << "step " << k << std::endl;
	  std::cout << " k1 " << k1 << std::endl;
	  std::cout << " k2 " << k2 << std::endl;
	  std::cout << " k3 " << k3 << std::endl;
	  std::cout << " k4 " << k4 << std::endl;
	  std::cout << "prec k1 " << k1.rel_distance(k1_old) << std::endl;
	  std::cout << "prec k2 " << k2.rel_distance(k2_old) << std::endl;
	  std::cout << "prec k3 " << k3.rel_distance(k3_old) << std::endl;
	  std::cout << "prec k4 " << k4.rel_distance(k4_old) << std::endl;
	  
	  if (k>100)
	  	break;
	  
	}while ((k1.rel_distance(k1_old) > atol)||
		(k2.rel_distance(k2_old) > atol)||
		(k3.rel_distance(k3_old) > atol)||
		(k4.rel_distance(k4_old) > atol));


	
	
      Affine2Vector k1_aff = Affine2Vector(k1,true);
      Affine2Vector k2_aff = Affine2Vector(k2,true);
      Affine2Vector k3_aff = Affine2Vector(k3,true);
      Affine2Vector k4_aff = Affine2Vector(k4,true);

      k1_aff = _ode->compute_derivatives_aff(1,*box_jn_aff + h*(a11*k1_aff  + a12*k2_aff + a13*k3_aff + a14*k4_aff));

      k2_aff = _ode->compute_derivatives_aff(1,*box_jn_aff + h*(a21*k1_aff + a22*k2_aff + a23*k3_aff + a24*k4_aff));
	
      k3_aff = _ode->compute_derivatives_aff(1,*box_jn_aff + h*(a21*k1_aff + a22*k2_aff + a33*k3_aff + a34*k4_aff));

      k4_aff = _ode->compute_derivatives_aff(1,*box_jn_aff + h*(a41*k1_aff + a42*k2_aff + a43*k3_aff + a44*k4_aff));

      Affine2Vector gauss8 = *box_jn_aff + h*( b1*k1_aff + b2*k2_aff + b3*k3_aff + b4*k4_aff);

	std::cout << "end of affine computation" << std::endl;
      return gauss8+*box_err_aff;

    };



  };
}

#endif
