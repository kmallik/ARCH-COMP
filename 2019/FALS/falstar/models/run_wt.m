function result = run_wt(u, T)
    Parameter = evalin('base', 'Parameter');
    v0_cell = evalin('base', 'v0_cell');

    iBin = evalin('base', 'iBin');
    iRandSeed = evalin('base', 'iRandSeed');

    % get the wind signal from indices and update Parameter
    Parameter.v0                        = v0_cell{iBin,iRandSeed};
    Parameter.v0.signals.values         = Parameter.v0.signals.values';
    Parameter.TMax                      = v0_cell{iBin,iRandSeed}.time(end);
    Parameter.Time.cut_out              = Parameter.Time.TMax;
    Parameter.v0_0 = Parameter.v0.signals.values(1);           
    Parameter = SimplifiedTurbine_ParamterFile(Parameter);
    assignin('base', 'Parameter', Parameter); 

    result = sim('SimpleWindTurbine_FalStar_aLVTS', 'StopTime', '630');
end