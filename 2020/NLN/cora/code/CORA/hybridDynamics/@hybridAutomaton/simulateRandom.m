function obj = simulateRandom(obj,params,options)
% simulateRandom - simulates a hybrid automata for random initial points
%                  and random inputs
%
% Syntax:  
%    obj = simulateRandom(obj,params,options)
%
% Inputs:
%    obj - hybrid automaton object
%    params - system parameters
%    options - settings for random simulation
%       .points - nr of simulation runs
%       .fracVert - fraction of initial states starting from vertices
%       .fracInpVert - fraction of input values taken from the 
%                       vertices of the input set
%       .inpChanges - number of times the input is changed in a simulation run
%
% Outputs:
%    obj - hybrid automaton object with stored simulation results
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Niklas Kochdumper
% Written:       03-December-2019 
% Last update:   08-May-2020 (MW, update interface)
% Last revision: ---

%------------- BEGIN CODE --------------

% check simulation options
options = params2options(params,options);
options = checkOptionsSimRandom(obj,options);

% initialize random inputs
u = cell(size(options.Uloc));

for j = 1:length(u)
   u{j} = randPoint(options.Uloc{j});
end

% determine random points inside the initial set
points = zeros(dim(options.R0),options.points);
counter = 1;

for i = 1:options.points
   if counter < options.fracVert * options.points
      points(:,i) = randPointExtreme(options.R0); 
   else
      points(:,i) = randPoint(options.R0); 
   end

   counter = counter + 1;
end

t = linspace(options.tStart,options.tFinal,options.inpChanges);

startLoc = options.startLoc;

% simulate the hybrid automaton
for i = 1:options.points

   counter = 1;
   loc = startLoc;

   % loop over all input changes
   for g = 1:length(t)-1

       % compute random input
       optsSim.u = u;

       if counter < options.inpChanges * options.fracInpVert 
           optsSim.u{loc} = randPointExtreme(options.Uloc{loc});
       else
           optsSim.u{loc} = randPoint(options.Uloc{loc});
       end              

       % simulate hybrid automaton
       optsSim.x0 = points(:,i);
       if t(g) ~= 0
           optsSim.tStart = t(g);
       end
       optsSim.tFinal = t(g+1);
       optsSim.startLoc = loc;
       optsSim.finalLoc = options.finalLoc;

       obj = simulate(obj,optsSim);

       % update location and initial point
       points(:,i) = obj.result.simulation.x{end}(end,:)';
       loc = obj.result.simulation.location(end);
       optsSim = [];
       counter = counter + 1;
   end
end
    
    
end

%------------- END OF CODE --------------