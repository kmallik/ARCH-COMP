function res = test_Krylov_homogeneousSolution_iss(~)
% test_Krylov_homogeneousSolution_iss - unit_test_function for checking
% the Krylov method for the homegeneous solution of an initial set; larger
% ISS system used
%
% Syntax:  
%    res = test_Krylov_homogeneousSolution_iss(~)
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      23-August-2017
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% load system matrices
load('iss.mat');
dim = length(A);

% initial set
options.R0 = zonotope(interval(-0.0001*ones(dim,1),0.0001*ones(dim,1)));


%set options --------------------------------------------------------------
options.timeStep = 0.01; %time step size for reachable set computation
options.tFinal = options.timeStep;

options.x0 = center(options.R0);
options.U = zonotope(zeros(3,1));
options.uTrans = zeros(3,1);
options.taylorTerms = 6;
options.KrylovError = eps;
options.reductionTechnique = 'girard';
options.zonotopeOrder = 1;
options.KrylovStep = 1;
%--------------------------------------------------------------------------

%specify continuous dynamics-----------------------------------------------
linDyn = linearSys('iss',A,B);
%--------------------------------------------------------------------------

% compute overapproximation
expFactor = 1+1e-17;
[Rnext, options] = initReach_Krylov(linDyn, options.R0, options);
Rnext_box = interval(Rnext.tp);
Rnext_box = enlarge(Rnext_box,expFactor);

% compute exact solution
R_exact = expm(A*options.timeStep)*options.R0;
R_exact_box = interval(R_exact);

% Is exact solution in zonotope?
res = (R_exact_box <= Rnext_box);

%------------- END OF CODE --------------