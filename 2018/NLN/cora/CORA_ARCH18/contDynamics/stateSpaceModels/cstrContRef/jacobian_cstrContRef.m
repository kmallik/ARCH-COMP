function [A,B]=jacobian_cstrContRef(x,u,p)

A{1}=[- 72000000000*exp(-8750/(x(2) + 350)) - 1,-(630000000000000*exp(-8750/(x(2) + 350))*(x(1) + 1/2))/(x(2) + 350)^2,0,0;...
(7712133891213389*exp(-8750/(x(2) + 350)))/512,(33740585774058576875*exp(-8750/(x(2) + 350))*(x(1) + 1/2))/(256*(x(2) + 350)^2) - 739/239,0,0;...
0,0,- 72000000000*exp(-8750/(x(4) + 350)) - 1,-(630000000000000*exp(-8750/(x(4) + 350))*(x(3) + 1/2))/(x(4) + 350)^2;...
0,0,(7712133891213389*exp(-8750/(x(4) + 350)))/512,(33740585774058576875*exp(-8750/(x(4) + 350))*(x(3) + 1/2))/(256*(x(4) + 350)^2) - 739/239];

A{2}=[0,0,0,0;...
500/239,0,-500/239,0;...
0,0,0,0;...
0,0,0,0];

A{3}=[0,0,0,0;...
0,500/239,0,-500/239;...
0,0,0,0;...
0,0,0,0];

A{4}=[0,0,0,0;...
0,0,0,0;...
0,0,0,0;...
0,0,0,0];

B{1}=[1,0;...
0,1;...
0,0;...
0,0];

B{2}=[0,0;...
0,0;...
0,0;...
0,0];

B{3}=[0,0;...
0,0;...
0,0;...
0,0];

B{4}=[0,0;...
0,0;...
0,0;...
0,0];

