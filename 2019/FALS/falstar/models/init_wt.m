addpath(genpath('../shared/benchmarks/wind-turbine'));

SimplifiedTurbine_Config;

%load wind files
load('ClassA.mat')
load('ClassA_config.mat')
load('aeromaps3.mat');

% remove all unnecessary fields (otherwise Simulink will throw an error)
cT_modelrm = rmfield(cT_model,{'VarNames'});%,'RMSE','ParameterVar','ParameterStd','R2','AdjustedR2'});
cP_modelrm = rmfield(cP_model,{'VarNames'});%,'RMSE','ParameterVar','ParameterStd','R2','AdjustedR2'});

% initialize WAFO
initwafo

Parameter.InitialConditions = load('InitialConditions');

Parameter.Time.TMax                 = 630;
Parameter.Time.dt                   = 0.01;
Parameter.Time.cut_in               = 30;

load_system('SimpleWindTurbine_FalStar_aLVTS')
