function [Tf,ind] = thirdOrderTensorInt_freeRigidBodyparamEq(x,u)



 Tf{1,1} = interval(sparse(6,6),sparse(6,6));



 Tf{1,2} = interval(sparse(6,6),sparse(6,6));



 Tf{1,3} = interval(sparse(6,6),sparse(6,6));



 Tf{1,4} = interval(sparse(6,6),sparse(6,6));



 Tf{1,5} = interval(sparse(6,6),sparse(6,6));



 Tf{1,6} = interval(sparse(6,6),sparse(6,6));



 Tf{2,1} = interval(sparse(6,6),sparse(6,6));



 Tf{2,2} = interval(sparse(6,6),sparse(6,6));



 Tf{2,3} = interval(sparse(6,6),sparse(6,6));



 Tf{2,4} = interval(sparse(6,6),sparse(6,6));



 Tf{2,5} = interval(sparse(6,6),sparse(6,6));



 Tf{2,6} = interval(sparse(6,6),sparse(6,6));



 Tf{3,1} = interval(sparse(6,6),sparse(6,6));



 Tf{3,2} = interval(sparse(6,6),sparse(6,6));



 Tf{3,3} = interval(sparse(6,6),sparse(6,6));



 Tf{3,4} = interval(sparse(6,6),sparse(6,6));



 Tf{3,5} = interval(sparse(6,6),sparse(6,6));



 Tf{3,6} = interval(sparse(6,6),sparse(6,6));


 ind = cell(3,1);
 ind{1} = [];


 ind{2} = [];


 ind{3} = [];

